const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('db.json')
const middlewares = jsonServer.defaults({"logger":true})
const cors = require("cors")
// Set default middlewares (logger, static, cors and no-cache)
// Add custom routes before JSON Server router

server.use(middlewares)

server.get('/echo', (req, res) => {
  res.jsonp("echo")
})

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser)

server.use(
  cors({
      origin: true,
      credentials: true,
      preflightContinue: false,
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  })
);
server.options('*', cors());

// Use default router
server.use(router)

server.listen(3000, () => {
  console.log('JSON Server is running')
})