import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <a href="https://ulissesnewdev.netlify.app/" alt="github link">
        Made By ulissesnew
      </a>
    </footer>
  );
};

export default Footer;
