import React, { useContext } from "react";
import "./Header.scss";
import { Link, useLocation } from "react-router-dom";
import booksContext from "../../Context/booksContext";
import bookIcon from "../../assets/book.png";

const Header = () => {
  const location = useLocation();
  const data = useContext(booksContext);
  return (
    <ul className="header">
      <li className="header__left">
        <img className="table__icon" width="35px" src={bookIcon} alt="book" />
        <h2>Books</h2>
      </li>
      <div className="header__right">
        <li className="header__link">
          {location.pathname !== `/book/${data.id}` ? (
            <Link to="/">List of Books</Link>
          ) : null}
        </li>
        <li className="header__link">
          <Link to="/book">Register a Book</Link>
        </li>
      </div>
    </ul>
  );
};

export default Header;
