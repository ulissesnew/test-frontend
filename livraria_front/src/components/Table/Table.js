import React, { useContext, useState } from "react";
import "./Table.scss";
import Item from "./Item/Item";
import booksContext from "../../Context/booksContext";
import bookReadingIcon from "../../assets/book-reading.png";
import handIcon from "../../assets/hand-down.png";
import priceIcon from "../../assets/low-price.png";
import authorIcon from "../../assets/will.png";
import userIcon from "../../assets/user.png";

const Table = () => {
  const [search, setSearch] = useState("");
  const data = useContext(booksContext);
  const mapData = data.books.length
    ? data.books.map((book) => {
        return (
          <Item
            active={Number(search) === Number(book.id)}
            key={book.id}
            {...book}
            change={(e, id) => data.selectOption(e, book.id)}
            value={data.option}
          />
        );
      })
    : null;
  return (
    <div className="table__container">
      <table className="table">
        <caption className="table__caption">
          <img
            className="table__icon"
            width="35px"
            src={bookReadingIcon}
            alt="book"
          />
          Table of books.
          <div>
            <label className="table__label__search" htmlFor="search">
              Search with id
              <input
                id="search"
                className="table__search"
                type="number"
                onChange={(e) => setSearch(e.target.value)}
                value={search}
                min="1"
                autoComplete="off"
              />
            </label>
          </div>
        </caption>
        <thead>
          <tr>
            <th>
              <img
                className="table__icon"
                width="25px"
                src={userIcon}
                alt="book"
              />
              Name
            </th>
            <th>
              <img
                className="table__icon"
                width="25px"
                src={authorIcon}
                alt="book"
              />
              Author
            </th>
            <th className="table__price">
              <img
                className="table__icon"
                width="20px"
                src={priceIcon}
                alt="book"
              />
              Price
            </th>
            <th className="table__options">
              <img
                className="table__icon"
                width="20px"
                src={handIcon}
                alt="book"
              />
            </th>
          </tr>
        </thead>
        <tbody>{mapData}</tbody>
      </table>
    </div>
  );
};

export default Table;
