import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import BooksContext from "./booksContext";

const BookProvider = ({ children }) => {
  const history = useHistory();
  const [books, setBooks] = useState([]);
  const [book, setBook] = useState({
    name: "",
    author: "",
    price: 0,
    id: "",
  });
  const [option, setOption] = useState("");
  const [id, setId] = useState("");

  const selectOption = (e, bookId) => {
    setId(bookId);
    setOption(e.target.value);
  };

  const deleteBook = async (url, form) => {
    const options = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    try {
      const response = await fetch(url, options);
      await response.json();
      setOption("");
    } catch (error) {
      console.log(error);
      return [];
    }
  };
  const createBook = async (url, form) => {
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    try {
      const response = await fetch(url, options);
      const results = await response.json();
      setBooks(() => {
        return [results, ...books];
      });
      setOption("");
    } catch (error) {
      console.log(error);
      return [];
    }
  };
  const updateBook = async (url, form) => {
    const options = {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    };
    try {
      const response = await fetch(url, options);
      const results = await response.json();
      setOption("");
      return setBook(results);
    } catch (error) {
      console.log(error);
      return [];
    }
  };
  const getBook = (data, id) => {
    return data.length
      ? data.filter((book) => {
          return book.id === id;
        })
      : null;
  };
  const fetchBooks = async (url) => {
    try {
      const response = await fetch(url);
      const results = await response.json();
      return setBooks(results);
    } catch (error) {
      console.log(error);
      return [];
    }
  };
  const urlBooks = "http://localhost:3000/books?_sort=id&_order=asc";
  const urlDeleteBook = `http://localhost:3000/books/${id}`;

  useEffect(() => {
    fetchBooks(urlBooks);
    if (option === "edit") {
      history.push(`/book/${id}`);
    }
    if (option === "delete") {
      deleteBook(urlDeleteBook);
    }
  }, [option]);

  const booksState = {
    books: books,
    book: book,
    setBook: setBook,
    option: option,
    id: id,
    selectOption: selectOption,
    updateBook: updateBook,
    createBook: createBook,
    fetchBooks: fetchBooks,
    getBook: getBook,
  };
  return (
    <BooksContext.Provider value={booksState}>{children}</BooksContext.Provider>
  );
};

export default BookProvider;
