import { Switch, Route } from "react-router-dom";
import "./App.css";
import Table from "./components/Table/Table";
import BooksProvider from "./Context/BooksProvider";
import Form from "./components/Form/Form";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div className="App">
      <BooksProvider>
        <Header />
        <Switch>
          <Route path="/book/:id">
            <Form />
          </Route>
          <Route path="/book/">
            <Form />
          </Route>
          <Route path="/">
            <Table />
          </Route>
        </Switch>
        <Footer />
      </BooksProvider>
    </div>
  );
}

export default App;
