import "./Item.scss";

const Item = ({ change, name, author, price, option, active }) => {
  return (
    <tr className={active ? "item item--active" : "item"}>
      <td>{name}</td>
      <td>{author}</td>
      <td className="item__price">$ {Number(price).toFixed(2)}</td>
      <td className="item__select">
        <select
          onChange={change}
          className="select"
          value={option}
          autoComplete="off"
        >
          <option value="">?</option>
          <option className="select--edit" value="edit">
            Edit
          </option>
          <option className="select--delete" value="delete">
            Delete
          </option>
        </select>
      </td>
    </tr>
  );
};

export default Item;
