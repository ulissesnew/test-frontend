import React, { useContext, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import "./Form.scss";
import booksContext from "../../Context/booksContext";

const Form = () => {
  const [form, setForm] = useState({ name: "", author: "", price: "" });
  const handleForm = (e) => {
    const value = e.target.value;
    setForm({ ...form, [e.target.name]: value });
  };
  const history = useHistory();
  const location = useLocation();
  const data = useContext(booksContext);
  const urlUpdateBook = `http://localhost:3000/books/${data.id}`;
  const urlCreateBook = `http://localhost:3000/books`;

  let bookValues;
  if (location.pathname === `/book/${data.id}`) {
    bookValues = data.getBook(data.books, data.id);
  }
  const submitForm = () => {
    if (!form.name && !form.author && !form.price) {
      history.push("/");
    } else {
      location.pathname === "/book"
        ? data.createBook(urlCreateBook, form)
        : data.updateBook(urlUpdateBook, form);
      history.push("/");
    }
  };
  return (
    <form className="form" onSubmit={() => submitForm()}>
      <legend>
        {location.pathname === "/book"
          ? "Hello! Let's register a book."
          : "Update your book"}
      </legend>
      <label className="form__label" htmlFor="name">
        {" "}
        name
      </label>
      <input
        onChange={handleForm}
        className="form__input"
        id="name"
        name="name"
        type="text"
        placeholder={bookValues ? bookValues[0].name : "name"}
        required
        value={form.name}
        maxLength="30"
      />
      <label className="form__label" htmlFor="author">
        {" "}
        author
      </label>
      <input
        onChange={handleForm}
        className="form__input"
        id="author"
        name="author"
        type="text"
        placeholder={bookValues ? bookValues[0].author : "author"}
        required
        value={form.author}
        maxLength="30"
      />
      <label className="form__label" htmlFor="price">
        {" "}
        price
      </label>
      <input
        className="form__input"
        onChange={handleForm}
        id="price"
        name="price"
        type="number"
        placeholder={bookValues ? bookValues[0].price : "price"}
        required
        value={form.price}
        max="2000"
      />
      <button className="form__button">
        {location.pathname === "/book" ? "create book" : "update book"}
      </button>
    </form>
  );
};

export default Form;
